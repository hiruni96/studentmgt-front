import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AddFormComponent } from './students/add-form/add-form.component';
import { AllViewComponent } from './students/all-view/all-view.component';
import { StudentsComponent } from './students/students.component';
import { UpdateFormComponent } from './students/update-form/update-form.component';


const routes: Routes = [
  {
    path: 'students', component: StudentsComponent, children: [
      { path: 'add', component: AddFormComponent },
      { path: 'update', component: UpdateFormComponent },
      { path: '', component: AllViewComponent }
    ]
  },

  { path: 'login', component: LoginComponent },
  { path:'', redirectTo:'students', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
